from django.conf.urls import patterns, include, url
from django.contrib import admin

from api import urls as api_urls
from api.views.user_views import activate


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(api_urls)),
    url(r'^user/activate/(?P<hash>\w+)$', activate, name='activate_user')
)



