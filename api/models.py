# -*- coding: utf-8 -*-
from django.db import models

"""
Author: Alan Jeferson
Definitions of all the models of the application.
"""

"""
Represents a Jessica user.
"""
class JUser(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    password = models.CharField(max_length=32)
    creation_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    active = models.BooleanField(default=False)
    hash = models.CharField(max_length=32, unique=True)

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return dict(
            id = self.id,
            name  = self.name,
            email = self.email
        )

    class Meta:
        db_table = "juser"


"""
Represents everytime a user is asked to
fill in all the eight aspects.
This is a set of Snaps.
"""
class Jink(models.Model):
    date = models.DateTimeField("Jink date")
    juser = models.ForeignKey(JUser, verbose_name="JUser name")

    def __unicode__(self):
        return "Jink " + str(self.id) + " - " + str(self.date)

    class Meta:
        db_table = "jink"

"""
Represents one of the eight virtues.
"""
class Virtue(models.Model):
    virtue_names = (
        ("Altruism", "Altruism",),
        ("Compromise", "Compromise",),
        ("Forgiveness", "Forgiveness",),
        ("Honesty", "Honesty",),
        ("Humility", "Humility",),
        ("Niceness", "Niceness",),
        ("Patience", "Patience",),
        ("Respect", "Respect",),
    )

    name = models.CharField(choices=virtue_names, max_length=20, unique=True)

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return self.name

    class Meta:
        db_table = "virtue"
        ordering = ("name",)


"""
Represents one single attribute name and value.
Ex.: Forgiveness - 4.3
"""
class Snap(models.Model):
    name = models.ForeignKey(Virtue, related_name="snaps")
    value = models.DecimalField(max_digits=2, decimal_places=1)
    jink = models.ForeignKey(Jink, related_name='snaps')

    def __unicode__(self):
        return "Jink " + str(self.jink.id) + " - " + self.name.__unicode__()

    class Meta:
        db_table = "snap"
        ordering = ("jink", "name", )



"""
Represents a mission an user can do.
It is general, not associated to a JUser.
"""
class Mission(models.Model):
    main_virtue = models.ForeignKey(Virtue, related_name="missions_main_virtue")
    other_virtues = models.ManyToManyField(Virtue, related_name="missions_other_virtues", blank=True)
    jups = models.IntegerField();

    def __unicode__(self):
        return '#' + str(self.id)

    def natural_key(self):
        virtues = [virtue.natural_key() for virtue in self.other_virtues.all()]
        print virtues
        return dict(
            id = self.id,
            jups = self.jups,
            objective = self.objective,
            main_virtue = self.main_virtue.natural_key(),
            other_virtues = virtues)

    class Meta:
        db_table = "mission"

"""
Model which represents a single translation of a mission.
"""
class Location(models.Model):
    name = models.CharField(max_length=50)
    language = models.CharField(max_length=2)
    country = models.CharField(max_length=2)

    def __unicode__(self):
        return self.name


"""
Model which relates both mission and language.
"""
class MissionTranslation(models.Model):
    objective = models.CharField(max_length=200)
    location = models.ForeignKey(Location, related_name='locations')
    mission = models.ForeignKey(Mission, related_name='translations')

    def __unicode__(self):
        return '#' + str(self.mission.id) + ' - ' + self.objective


"""
Represents te relation beteween a mission
and a JUser.
"""
class UserMission(models.Model):
    status_values = (
        ("rejected", "Rejected"),
        ("current", "Current"),
        ("successful", "Successful"),
        ("unsuccessful", "Unsuccessful"),
        ("none", "None"),
        ("waiting", "Waiting"),
        ("tomorrow", "Tomorrow"),

    )
    description = models.TextField(max_length=500, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    status = models.CharField(max_length=20, choices=status_values)
    mission = models.ForeignKey(Mission)
    juser = models.ForeignKey(JUser)

    def __unicode__(self):
        return '#' + str(self.mission.id)

    class Meta:
        db_table = "user_mission"


"""
Represents a single setting of a user.
"""
class Setting(models.Model):
    key = models.CharField(max_length=20)
    value = models.CharField(max_length=20)
    juser = models.ForeignKey(JUser)

    def __unicode__(self):
        return self.key + ": " + self.value

    class Meta:
        db_table = "setting"
        ordering = ("juser", )

"""
Model used just for make it easy to return a JSON
response on views which does not have other returns
"""
class Success(models.Model):
    success = models.BooleanField(default=False)

    def __unicode__(self):
        return self.success