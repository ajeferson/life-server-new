# -*- coding: utf-8 -*-
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import Mission
from django.core import  serializers
from django.views.decorators.http import require_GET
from django.shortcuts import  get_object_or_404
from api.serializers.mission_serializer import MissionSerializer


"""
Mission related services.
"""

@api_view(['GET'])
def all(request):
    missions = Mission.objects.all()
    serializer = MissionSerializer(missions, many=True)
    return Response(serializer.data)

@require_GET
def get(request, mission_id):
    mission = get_object_or_404(Mission, id=mission_id)
    data = serializers.serialize('json', [mission], use_natural_keys=True)
    return HttpResponse(data, content_type='application/json')