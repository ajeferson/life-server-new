# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.utils.timezone import now
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import UserMission, Mission, JUser, Success
from django.core import serializers
from django.views.decorators.http import require_GET, require_POST
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
import json
from api.serializers.others import SuccessSerializer
from api.serializers.user_mission_serializer import UserMissionSerializer
from datetime import datetime

"""
UserMission related services.
"""

@require_GET
def all(request):
    user_missions = UserMission.objects.all()
    data = serializers.serialize('json', user_missions, use_natural_keys=True)
    return HttpResponse(data, content_type='application/json')


@require_GET
def get(request, user_mission_id):
    user_mission = get_object_or_404(UserMission, id=user_mission_id)
    data = serializers.serialize('json', [user_mission], use_natural_keys=True)
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
@require_POST
def insert(request):
    user_mission = UserMission(
        description = request.POST['description'],
        date = request.POST['date'],
        status = request.POST['status'],
        mission = request.POST['mission'],
        juser = request.POST['user']
    )
    user_mission.save()
    return HttpResponse(json.dumps({"success":"true"}), content_type='application/json')


@csrf_exempt
@require_POST
def remove(request):
    user_mission = get_object_or_404(UserMission, id=int(request.POST['id']))
    user_mission.delete()
    return HttpResponse(json.dumps({"success":"true"}), content_type='application/json')


@api_view(["POST"])
def edit(request):
    user_mission = get_object_or_404(UserMission, id=int(request.POST['id']))
    if 'description' in request.data:
        user_mission.description = request.POST['description']
    if 'date' in request.data:
        user_mission.date = datetime.strptime(request.POST['date'], '%d/%m/%Y %H:%M:%S')
    if 'status' in request.data:
        user_mission.status = request.POST['status']
    user_mission.save()
    success = Success(success=True)
    serializer = SuccessSerializer(success)
    return Response(serializer.data)


@api_view(['POST'])
def generate_for_user(request):
    user_id = request.POST["user_id"]
    quantity = request.POST["quantity"]
    user = JUser.objects.get(id=user_id)
    exclude = UserMission.objects.filter(juser__id=user_id).values_list('mission__id', flat=True)
    missions = Mission.objects.exclude(id__in=exclude).order_by("?")[:quantity]
    list_user_missions = []
    for mission in missions:
        user_misson = UserMission(mission=mission, juser=user, status="none");
        user_misson.save()
        list_user_missions.append(user_misson)

    serializer = UserMissionSerializer(list_user_missions, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def filter_for_user(request):
    user_missions = UserMission.objects.filter(juser__id=request.POST["user_id"])
    serializer = UserMissionSerializer(user_missions, many=True)
    return Response(serializer.data)