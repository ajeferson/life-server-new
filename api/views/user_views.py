# -*- coding: utf-8 -*-
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import JUser, Success
from django.core import serializers
from django.shortcuts import get_object_or_404, render_to_response
from django.views.decorators.http import require_POST, require_GET
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.core.urlresolvers import reverse
from django.conf import settings
import json
import hashlib
import datetime
import random
from api.serializers.others import SuccessSerializer

"""
User related services.
"""


@require_GET
def all(request):
    users = JUser.objects.all()
    data = serializers.serialize('json', users, fields=('name', 'email'))
    return HttpResponse(data, content_type='application/json')


@require_GET
def get(request, user_id):
    user = get_object_or_404(JUser, pk=user_id)
    data = serializers.serialize('json', [user], fields=('name', 'email'))
    return HttpResponse(data, content_type='application/json')


@require_GET
def remove(request, user_id):
    user = get_object_or_404(JUser, pk=user_id)
    user.delete()
    return HttpResponse(user_id+"")


@csrf_exempt
@require_POST
def insert(request):
    users = JUser.objects.filter(email=request.POST["email"])

    ## Checking if already is a user with the email
    if(len(users)>0):
        USER_NOT_ACTIVATED = -2;
        USER_ACTIVATED = -3;
        if(users[0].active):
            return HttpResponse(json.dumps({"id": str(USER_ACTIVATED)}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"id": str(USER_NOT_ACTIVATED)}), content_type="application/json")

    user = JUser(name=request.POST["name"],
                 email=request.POST["email"],
                 password=request.POST["password"])
    user.hash = hashlib.md5(user.email +
                            user.password +
                            str(random.randint) +
                            str(datetime.datetime.now())).hexdigest()
    user.save()

    ##Sending email
    subject, from_email, to = 'Life email confirmation', settings.EMAIL_HOST_USER, user.email
    html = get_template('email/activation_email.html')
    context = Context({
        'name' : user.name.split(' ')[0],
        'activation_url' : request.build_absolute_uri(reverse('activate_user', args=(user.hash,)))})
    html_content = html.render(context)
    msg = EmailMultiAlternatives(subject, '', from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    return HttpResponse(json.dumps({"id": str(user.id)}), content_type="application/json")


@api_view(["POST"])
def edit(request):
    user = JUser.objects.get(id=int(request.POST["id"]))
    if("name" in request.data):
        user.name = request.data["name"]
    if("email" in request.data):
        user.email = request.data["email"]
    if("password" in request.data):
        user.password = request.data["password"]
    user.save()
    success = Success(success=True)
    serializer = SuccessSerializer(success)
    return Response(serializer.data)


@require_GET
def activate(request, hash):
    user = get_object_or_404(JUser, hash=hash)
    if(user.active):
        return render_to_response("email/account_activated.html", locals())
    else:
        user.active = True
        user.save()
        return render_to_response("email/account_activated.html", locals())

def get_by_email(request):
    user = JUser.objects.filter(email=request.GET["email"])
    data = serializers.serialize('json', user, fields=('name', 'email', 'active', 'password'))
    return HttpResponse(data, content_type='application/json')