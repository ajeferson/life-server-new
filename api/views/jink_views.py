from django.http.response import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import Jink, Snap, Virtue, JUser
from api.serializers.jink_serializers import JinkSerializer
from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser
from api.serializers.snap_serializer import SnapSerializer
from datetime import datetime


__author__ = 'ajeferson'

def create_snap(request, name, jink):
    snap = Snap()
    snap.jink = jink
    snap.name = Virtue.objects.get(name=name)
    snap.value = request.data[name]
    snap.save()

@api_view(['POST'])
def insert(request):
    jink = Jink()
    jink.juser = JUser.objects.get(id=request.data['user_id'])
    jink.date = datetime.strptime(request.POST['date'], '%d/%m/%Y %H:%M:%S')
    jink.save()

    create_snap(request, 'Altruism', jink)
    create_snap(request, 'Compromise', jink)
    create_snap(request, 'Forgiveness', jink)
    create_snap(request, 'Honesty', jink)
    create_snap(request, 'Humility', jink)
    create_snap(request, 'Niceness', jink)
    create_snap(request, 'Patience', jink)
    create_snap(request, 'Respect', jink)

    serializer = JinkSerializer(jink);
    return Response(serializer.data)

@api_view(['GET'])
def all(request):
    jinks = Jink.objects.all();
    serializer = JinkSerializer(jinks,many=True);
    return Response(serializer.data)

@api_view(['POST'])
def getByUser(request):
    jinks = Jink.objects.filter(juser__id=request.data['user_id']).order_by('-date');
    serializer = JinkSerializer(jinks,many=True);
    return Response(serializer.data)