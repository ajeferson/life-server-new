from rest_framework.relations import StringRelatedField

__author__ = 'ajeferson'

from api.models import Snap
from rest_framework import serializers



class SnapSerializer(serializers.ModelSerializer):

    name = serializers.StringRelatedField()

    class Meta:
        model = Snap
        fields = ('name','value',)