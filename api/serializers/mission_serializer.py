__author__ = 'ajeferson'

from api.serializers.mission_translation_serializer import MissionTranslationSerializer
from rest_framework import serializers
from api.models import Mission

"""
Used to serialize a mission.
"""
class MissionSerializer(serializers.ModelSerializer):

    other_virtues = serializers.StringRelatedField(many=True)
    main_virtue = serializers.StringRelatedField()
    translations = MissionTranslationSerializer(many=True)

    class Meta:
        model = Mission
        fields = ('id', 'jups', 'main_virtue', 'other_virtues', 'translations',)