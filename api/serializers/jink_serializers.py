from api.serializers.snap_serializer import SnapSerializer
from datetime import datetime

__author__ = 'ajeferson'

from rest_framework import serializers
from api.models import Jink

class JinkSerializer(serializers.ModelSerializer):


    date_formatted = serializers.SerializerMethodField('date_format')

    def date_format(self, obj):
        return str(datetime.strptime(str(obj.date)[:19], "%Y-%m-%d %H:%M:%S"))

    snaps = SnapSerializer(many=True)

    class Meta:
        model = Jink
        fields = ('date_formatted','juser','snaps',)

