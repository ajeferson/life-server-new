from api.models import UserMission
from api.serializers.mission_serializer import MissionSerializer
from datetime import datetime

__author__ = 'ajeferson'

from rest_framework import serializers

class UserMissionSerializer(serializers.ModelSerializer):

    mission = MissionSerializer()
    date_formatted = serializers.SerializerMethodField('date_format')

    def date_format(self, obj):
        return str(datetime.strptime(str(obj.date)[:19], "%Y-%m-%d %H:%M:%S"))


    class Meta:
        model = UserMission
        fields = ('id', 'mission', 'date_formatted', 'status')
