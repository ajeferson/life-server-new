from api.models import Success

__author__ = 'ajeferson'

from rest_framework import serializers

"""
Used to serialize the success message
"""
class SuccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Success
        fields = ('success',)
