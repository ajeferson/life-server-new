__author__ = 'ajeferson'

from api.serializers.location_serializers import LocationSerializer
from rest_framework import serializers
from api.models import MissionTranslation

"""
Serializes a mission with the respective locations
"""
class MissionTranslationSerializer(serializers.ModelSerializer):

    location = LocationSerializer()

    class Meta:
        model = MissionTranslation
        fields = ('location', 'objective',)