__author__ = 'ajeferson'
from rest_framework import serializers
from api.models import Virtue

"""
Serializer for virtues.
"""
class VirtueSerializer(serializers.StringRelatedField):
    class Meta:
        model = Virtue
        fields = ('name',)
