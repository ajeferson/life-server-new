# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(verbose_name=b'Jink date')),
            ],
            options={
                'db_table': 'jink',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=200)),
                ('password', models.CharField(max_length=32)),
                ('creation_date', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('active', models.BooleanField(default=False)),
                ('hash', models.CharField(unique=True, max_length=32)),
            ],
            options={
                'db_table': 'juser',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('objective', models.TextField(max_length=200)),
                ('jups', models.IntegerField()),
            ],
            options={
                'db_table': 'mission',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=20)),
                ('value', models.CharField(max_length=20)),
                ('juser', models.ForeignKey(to='api.JUser')),
            ],
            options={
                'ordering': ('juser',),
                'db_table': 'setting',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Snap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.DecimalField(max_digits=2, decimal_places=1)),
                ('jink', models.ForeignKey(to='api.Jink')),
            ],
            options={
                'ordering': ('jink', 'name'),
                'db_table': 'snap',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserMission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(max_length=500, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=20, choices=[(b'rejected', b'Rejected'), (b'current', b'Current'), (b'successful', b'Successful'), (b'unsuccessful', b'Unsuccessful'), (b'none', b'None')])),
                ('juser', models.ForeignKey(to='api.JUser')),
                ('mission', models.ForeignKey(to='api.Mission')),
            ],
            options={
                'db_table': 'user_mission',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Virtue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=20, choices=[(b'Altruism', b'Altruism'), (b'Compromise', b'Compromise'), (b'Forgiveness', b'Forgiveness'), (b'Honesty', b'Honesty'), (b'Humility', b'Humility'), (b'Niceness', b'Niceness'), (b'Patience', b'Patience'), (b'Respect', b'Respect')])),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'virtue',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='snap',
            name='name',
            field=models.ForeignKey(related_name='snaps', to='api.Virtue'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mission',
            name='main_virtue',
            field=models.ForeignKey(related_name='missions_main_virtue', to='api.Virtue'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mission',
            name='other_virtues',
            field=models.ManyToManyField(related_name='missions_other_virtues', to='api.Virtue', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jink',
            name='juser',
            field=models.ForeignKey(verbose_name=b'JUser name', to='api.JUser'),
            preserve_default=True,
        ),
    ]
