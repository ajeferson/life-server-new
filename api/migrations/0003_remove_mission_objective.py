# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_location_missiontranslation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mission',
            name='objective',
        ),
    ]
