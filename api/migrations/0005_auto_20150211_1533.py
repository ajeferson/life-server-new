# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20150122_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='success',
            name='success',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usermission',
            name='status',
            field=models.CharField(max_length=20, choices=[(b'rejected', b'Rejected'), (b'current', b'Current'), (b'successful', b'Successful'), (b'unsuccessful', b'Unsuccessful'), (b'none', b'None'), (b'waiting', b'Waiting'), (b'tomorrow', b'Tomorrow')]),
            preserve_default=True,
        ),
    ]
