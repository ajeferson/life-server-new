# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_remove_mission_objective'),
    ]

    operations = [
        migrations.CreateModel(
            name='Success',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('success', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='missiontranslation',
            name='location',
            field=models.ForeignKey(related_name='locations', to='api.Location'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='missiontranslation',
            name='mission',
            field=models.ForeignKey(related_name='translations', to='api.Mission'),
            preserve_default=True,
        ),
    ]
