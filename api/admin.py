# -*- coding: utf-8 -*-
from django.contrib import admin
from api.models import *


"""
Defines how the admin page for each models
should appear on the admin site.
Author: Alan Jeferson
"""

admin.site.register(Virtue)

class UserMissionAdmin(admin.ModelAdmin):
    list_display = ("juser", "mission", "date", "status",)
    list_filter = ["status", "date"]
    search_fields = ["description", "juser__name", "mission__objective"]
admin.site.register(UserMission, UserMissionAdmin)

class JUserAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "password", "hash", "active")
    list_filter = ["creation_date"]
    search_fields = ["name", "email"]
    readonly_fields = ("hash",)
admin.site.register(JUser, JUserAdmin)

class JinkAdmin(admin.ModelAdmin):
    list_display = ("juser", "date",)
    list_filter = ["date"]
    search_fields = ["juser__name"]
admin.site.register(Jink, JinkAdmin)

class SnapAdmin(admin.ModelAdmin):
    list_display = ("name", "jink", "value", )
    list_filter = ["name"]
    search_fields = ["name__name"]
admin.site.register(Snap, SnapAdmin)

class SettingAdmin(admin.ModelAdmin):
    list_display = ("juser", "key", "value")
    list_filter = ["key"]
    search_fields = ["key", "juser__name"]
admin.site.register(Setting, SettingAdmin)

class LocationAdmin(admin.ModelAdmin):
    list_display = ("name", "language", "country")
    search_fields = ["language", "country"]
admin.site.register(Location, LocationAdmin)

class MissionTranslationAdmin(admin.ModelAdmin):
    list_display = ("objective", "location")
    list_filter = ["location"]
admin.site.register(MissionTranslation, MissionTranslationAdmin)

class MissionTranslationInline(admin.StackedInline):
    model = MissionTranslation
    extra = 1

class MissionAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "main_virtue", "jups")
    list_filter = ["main_virtue"]
    search_fields = ["main_virtue__name"]
    inlines = [MissionTranslationInline]
admin.site.register(Mission, MissionAdmin)