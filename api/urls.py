# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from api.views import mission_views, user_views, user_mission_views, jink_views


urlpatterns = patterns('',
    ##User urls
    url(r'^user/insert$', user_views.insert, name='insert_user'),
    url(r'^user/edit$', user_views.edit, name='edit_user'),
    url(r'^user/all$', user_views.all, name='all_users'),
    url(r'^user/get/(?P<user_id>\d+)$', user_views.get, name='get_user'),
    url(r'^user/get$', user_views.get_by_email, name='get_user_by_email'),
    url(r'^user/remove/(?P<user_id>\d+)$', user_views.remove, name='remove_user'),

    ##Mission urls
    url(r'^mission/all$', mission_views.all, name='all_missions'),
    url(r'^mission/get/(?P<mission_id>\d+)$', mission_views.get, name='get_mission'),

    ##Jink urls
    url(r'^jink/all$',jink_views.all,name='all_jink'),
    url(r'^jink/insert$',jink_views.insert,name='insert_jink'),
    url(r'^jink/getByUser$',jink_views.getByUser,name='get_jink'),


    ##UserMission urls
    url(r'^user_mission/all$', user_mission_views.all, name='all_user_missions'),
    url(r'^user_mission/get/(?P<user_mission_id>\d+)$', user_mission_views.get, name='get_user_mission'),
    url(r'^user_mission/insert$', user_mission_views.insert, name='insert_user_mission'),
    url(r'^user_mission/remove$', user_mission_views.remove, name='remove_user_mission'),
    url(r'^user_mission/edit$', user_mission_views.edit, name='edit_user_mission'),
    url(r'^user_mission/generate_for_user$', user_mission_views.generate_for_user, name='generate_for_user'),
    url(r'^user_mission/filter_for_user$', user_mission_views.filter_for_user, name='filter_for_user'),
)